# Hello world project

## To build project

1. mkdir build
2. cd build
3. cmake ..
4. make

## To run the project

```
$ build/HelloWorld
```
