#include <iostream>

int main(int argc, char * argv[])
{
    std::cout << "Hello world" << std::endl;

    if (argc > 1)
    {
        for (int i = 1; i < argc; i++)
        {
            std::cout << i << ": " << argv[i] << std::endl;
        }
    }
}
